#ifndef ARRAY_H
#define ARRAY_H


class Arrays
{
private:

    int array_[500];
    int arraylength_,leastinteger_;
    int min_;
    int max_;
public:
    Arrays();
    void setArraylength(int);
    int getArraylength();
    void setLeastinteger(int);
    int getLeastinteger();
    void setArray();
    int getArray();
 //END of Setters and Getters
    void reciever();
    void minmaxFinder();
    void sort();
    void search();
    void display();
    void run();
};

#endif // ARRAY_H
