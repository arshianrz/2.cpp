#include "array.h"
#include <iostream>

using namespace std;



Arrays::Arrays()
{
    //Also we could use our run() function here.
}
//****************************************************************************************************************

void Arrays::setArraylength(int N)
{
    arraylength_=N;
}

int Arrays::getArraylength()
{
    return arraylength_;
}
//****************************************************************************************************************
void Arrays::setLeastinteger(int M)
{
    leastinteger_=M;
}

int Arrays::getLeastinteger()
{
    return leastinteger_;
}
//***************************************************************************************************************
void Arrays::setArray()
{
    array_[getArraylength()];
}

int Arrays::getArray()
{
    return array_[getArraylength()];
}

//****************************************************************************************************************
void Arrays::reciever()
{

    for( int i=0 ; i<=(arraylength_-1) ; i++)
    {
        cout<<"Array's ["<<i<<"] place is :";
        cin>> array_[i];
        cout<<endl;

        if(array_[i] < leastinteger_)
        {
                while(array_[i]<leastinteger_)
                {
                cout<<"Please try again . \n\n";
                cout<<"Array's ["<<i<<"] place is :";
                cin>> array_[i];
                cout<<endl;
                }
        }

    }
    cout<<"The array you've entered is \n{ ";
    display();
}
//****************************************************************************************************************
void Arrays::minmaxFinder()
{
    max_=array_[0];

    for(int i=arraylength_-1 ; i>=0 ; i--) //Finding Maximum.
    {
        if(array_[i]>max_)
        {
            max_=array_[i];
        }
    }
    cout<<"\n\nMaximum is: "<<max_<<". \n";

    min_=array_[1];

    for(int i=arraylength_-1 ; i>=0 ; i--) //Finding Minimum.
    {
        if(array_[i]<min_)
        {
            min_=array_[i];
        }
    }
    cout<<"Minimum is: "<<min_<<". \n\n";
}

//****************************************************************************************************************

void Arrays::sort()
{
    int temp;
    for(int i=0 ; i<arraylength_-1 ; i++)
    {
            for(int j=0 ; j<arraylength_-i-1 ; j++)
            {
                if (array_[j]>array_[j+1])
                {
                    temp=array_[j];
                    array_[j]=array_[j+1];
                    array_[j+1]=temp;
                }
            }
    }
    cout<<"\nSorted array is { ";
    display();
}

//****************************************************************************************************************

void Arrays::search()
{
    int x,flag;

    char ans='y';

    while(ans=='y')
    {
        flag=0;
        cout<<"Enter a number to search.\n";
        cin>>x;
        for(int i=0 ; i<=(arraylength_-1) ; i++)
        {
            if (array_[i]==x)
            {
                flag=1;
            }
        }
        if (flag==1)
        {
            cout<<"Number exists in array.\n\n";
        }
        else if(flag==0)
        {
            cout<<"Number doesn't exist in array.\n\n";
        }

        cout<<"Do you want to try again? (y/n) \n";
        cin>>ans;
    }
}
//****************************************************************************************************************
void Arrays::display( )
{
    for(int i=arraylength_-1 ; i>=0 ; i--)
    {
        cout<<array_[i];
        if(i==0)
        {
            cout<<" }";
        }
        else
            cout<<" , ";
    }
    cout<<"\n\n";

}

//****************************************************************************************************************

void Arrays::run()
{
    int x,y;
    cout<<"How many Numbers you want to enter ? \n";
    cin>>x;
    cout<<"Enter a number. \n*NOTE ! \n*Any Number you insert in array must be bigger than that. \n\n";
    cin>>y;

    setArraylength(x);
    getArraylength();
    setLeastinteger(y);
    getLeastinteger();
    setArray();
    getArray();
    reciever();
    minmaxFinder();
    sort();
    search();

}
//****************************************************************************************************************
